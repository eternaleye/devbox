#!/usr/bin/env bash
set -e

if [ $(whoami) = "root" ]; then
	exec sudo -u vagrant -i /vagrant/bootstrap.sh
fi

mkdir .ssh 2>/dev/null || true
echo "gitlab.com,52.21.36.51 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=" >> ~/.ssh/known_hosts
echo "gitlab.com,104.210.2.228 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=" >> ~/.ssh/known_hosts
sudo sed --in-place /etc/apt/sources.list -e 's/archive.ubuntu.com/mirror.clarkson.edu/g'
sudo sed --in-place /etc/apt/sources.list -e 's/security.ubuntu.com/mirror.clarkson.edu/g'
sudo dpkg --add-architecture i386
sudo apt-get update -y && sudo apt-get upgrade -y && sudo apt-get dist-upgrade -y
sudo apt-get install -y python python-pip qemu-system libxml2 build-essential \
    git clang llvm gcc-arm-linux-gnueabi gcc-arm-linux-gnueabihf libc6-dev-i386
sudo pip install --upgrade tempita

sed -i '1i export RUST_TARGET_PATH=/vagrant/sel4-targets' ~/.bashrc
sed -i '1i export PATH=/home/vagrant/.cargo/bin:$PATH' ~/.bashrc
source ~/.bashrc

pushd /vagrant >/dev/null
bash get_new_repos.sh
popd >/dev/null

if hash rustc 2>/dev/null; then
    true
else
    curl -sSOf https://static.rust-lang.org/rustup.sh
    sudo bash rustup.sh --yes --channel=nightly --date=2016-04-22 --prefix=/usr
fi
RUSTC_REVISION=$(rustc -vV | awk '/commit-hash/ { print $2; }')
RUSTC_DATE=$(rustc -vV | awk '/commit-date/ { print $2; }')
RUSTC_DIR=~/rust-$RUSTC_DATE/rustc-nightly

if [ ! -e rust-$RUSTC_DATE.stamp ]; then
    mkdir rust-$RUSTC_DATE
    pushd rust-$RUSTC_DATE >/dev/null
    wget --quiet https://static.rust-lang.org/dist/$RUSTC_DATE/rustc-nightly-src.tar.gz
    tar xf rustc-nightly-src.tar.gz
    touch ../rust-$RUSTC_DATE.stamp
    popd >/dev/null
fi

# arg 1 is the rust triple
# arg 2 is the name of the rust library.
# arg 3 and onward is any extra rust flags.
build_rust_lib() {
    if [ "$(cat /usr/lib/rustlib/$1/lib/$2.rlib.stamp 2>/dev/null)" != "$RUSTC_REVISION" ]; then
        pushd src/$2 >/dev/null
        echo rustc -O --target=$1 ${@:3} $(pwd)/lib.rs
        rustc -O --target=$1 ${@:3} $(pwd)/lib.rs
        sudo cp $2.rlib /usr/lib/rustlib/$1/lib
        echo $RUSTC_REVISION | sudo tee /usr/lib/rustlib/$1/lib/$2.rlib.stamp > /dev/null
        popd >/dev/null
    fi
}

# arg 1 is the rust triple
# arg 2 is the corresponding llvm triple
# arg 3 is the CFLAGS to use when building compiler-rt
# arg 4 is the name of the subdirectory of compiler-rt's build dir that
#          contains the final artifacts.
make_target_libs() {
    sudo mkdir -p /usr/lib/rustlib/$1/lib
    pushd $RUSTC_DIR >/dev/null
    if [ "$(cat /usr/lib/rustlib/$1/lib/libcompiler-rt.a.stamp 2>/dev/null)" != "$RUSTC_REVISION" ]; then
        CC=clang CXX=clang++ ./configure --target=$2
        make $2/rt/libcompiler-rt.a
        sudo cp $2/rt/libcompiler-rt.a /usr/lib/rustlib/$1/lib/libcompiler-rt.a
        echo $RUSTC_REVISION | sudo tee /usr/lib/rustlib/$1/libcompiler-rt.a.stamp > /dev/null
    fi
    build_rust_lib $1 libcore
    build_rust_lib $1 librustc_unicode
    build_rust_lib $1 liballoc --cfg feature=\"external_funcs\"
    build_rust_lib $1 libcollections
    build_rust_lib $1 librand

    popd >/dev/null
}

make_target_libs i686-sel4-unknown i686-unknown-linux-gnu
make_target_libs arm-sel4-gnueabi arm-unknown-linux-gnueabi
make_target_libs arm-sel4-gnueabihf arm-unknown-linux-gnueabihf
